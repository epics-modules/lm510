alias("${CONTROLLER}${R}CommsOK",             "${P}${R}CommsOK")
alias("${CONTROLLER}${R}DisconnectTrigR",     "${P}${R}DisconnectTrigR")
alias("${CONTROLLER}${R}CtrlModeS",           "${P}${R}CtrlModeS")
alias("${CONTROLLER}${R}CtrlMode-RB",         "${P}${R}CtrlMode-RB")
alias("${CONTROLLER}${R}CommsCtrlMode",       "${P}${R}CommsCtrlMode")


record(longin, "${P}${R}ChanR")
{
    field(DESC, "The channel number")
    field(VAL,  "${CH}")
    field(PINI, "YES")
    field(DISP, "1")
}


record(stringout, "${P}${R}DevNameR")
{
    field(DESC, "Device name")
    field(VAL,  "${P}")
    field(OUT,  "${CONTROLLER}${R}Ch${CH}DevNameR PP")
    field(PINI, "YES")
    field(DISP, "1")
}


record(stringin, "${P}${R}ControllerR")
{
    field(DESC, "Controller's name")
    field(VAL,  "${CONTROLLER}")
    field(PINI, "YES")
    field(DISP, "1")
}


record(mbbi, "${P}${R}Unit-RB")
{
    field(DESC, "Get unit")
    field(DTYP, "stream")
    field(INP,  "@lm510.proto units_get(${P}${R},ChanR,#esrUnitRB) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-RB")

    field(ZRVL, "0")
    field(ZRST, "cm")

    field(ONVL, "1")
    field(ONST, "in")

    field(TWVL, "2")
    field(TWST, "%")

    field(FLNK, "${P}${R}#UnitStr")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrUnitRB, normalPV=Unit-RB"
include "lm510_check_esr.template"


record(stringin, "${P}${R}#UnitStr")
{
    field(INP,  "${P}${R}Unit-RB")
}


record(sseq, "${P}${R}#EGUChRec")
{
    field(DESC, "Detect change of EGU")
    field(DOL1, "${P}${R}#UnitStr CPP")
    field(LNK1, "${P}${R}HighAlrmThreshS.EGU NPP")

    field(DOL2, "${P}${R}#EGUChRec.STR1")
    field(LNK2, "${P}${R}HighCtrlThreshS.EGU NPP")

    field(DOL3, "${P}${R}#EGUChRec.STR1")
    field(LNK3, "${P}${R}LowAlrmThreshS.EGU NPP")

    field(DOL4, "${P}${R}#EGUChRec.STR1")
    field(LNK4, "${P}${R}LowCtrlThreshS.EGU NPP")

    field(DISV, "1")
    field(SDIS, "${P}${R}Unit-RB.UDF")
}


record(mbbo, "${P}${R}UnitS")
{
    field(DESC, "Set unit")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto units_set(${P}${R},ChanR,#esrUnitS) ${ASYNPORT}")

    field(ZRVL, "0")
    field(ZRST, "cm")

    field(ONVL, "1")
    field(ONST, "in")

    field(TWVL, "2")
    field(TWST, "%")

    field(FLNK, "${P}${R}Unit-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrUnitS, normalPV=UnitS"
include "lm510_check_esr.template"


record(bi, "${P}${R}TypeR")
{
    field(DESC, "Type of sensor")
    field(DTYP, "stream")
    field(INP,  "@lm510.proto chan_type_get(${P}${R},ChanR) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-RB")

    field(ZNAM, "Liquid Helium Level")
    field(ONAM, "Liquid Nitrogen Level")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(fanout, "${P}${R}#Ch${CH}Stat-FO")
{
    field(LNK1, "${P}${R}ReadStatR")
    field(LNK2, "${P}${R}CtrlStatR")
    field(LNK3, "${P}${R}CtrlTimoR")
    field(LNK4, "${P}${R}InhStatR")
    field(LNK5, "${P}${R}AlrmStatR")
    field(LNK6, "${P}${R}LHeSensStatR")
    field(LNK7, "${P}${R}BurnOutStatR")

    field(DISV, "0")
    field(SDIS, "${P}${R}DisconnectTrigR CP")
}


record(bi, "${P}${R}ReadStatR")
{
    field(DESC, "Read in progress bit")
    field(INP,  "${CONTROLLER}${R}#Ch${CH}StatR.B0 CP MSS")

    field(ZNAM, "Not reading")
    field(ONAM, "Read in progress")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(bi, "${P}${R}CtrlStatR")
{
    field(DESC, "Control (refill) active bit")
    field(INP,  "${CONTROLLER}${R}#Ch${CH}StatR.B1 CP MSS")

    field(ZNAM, "Control inactive")
    field(ONAM, "Control /refill active")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(bi, "${P}${R}CtrlTimoR")
{
    field(DESC, "Control timeout bit")
    field(INP,  "${CONTROLLER}${R}#Ch${CH}StatR.B2 CP MSS")

    field(ZNAM, "No timeout")
    field(ONAM, "Control timeout")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(bi, "${P}${R}InhStatR")
{
    field(DESC, "Control (refill) inhibited bit")
    field(INP,  "${CONTROLLER}${R}#Ch${CH}StatR.B3 CP MSS")

    field(ZNAM, "No inhibit")
    field(ONAM, "Control /refill inhibited")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(bi, "${P}${R}AlrmStatR")
{
    field(DESC, "Alarm limit exceeded active bit")
    field(INP,  "${CONTROLLER}${R}#Ch${CH}StatR.B4 CP MSS")

    field(ZNAM, "No alarm")
    field(ONAM, "Alarm limit exceeded")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(bi, "${P}${R}LHeSensStatR")
{
    field(DESC, "Open sensor (LHe) bit")
    field(INP,  "${CONTROLLER}${R}#Ch${CH}StatR.B5 CP MSS")

    field(ZNAM, "Sensor nominal")
    field(ONAM, "Open sensor detected")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(bi, "${P}${R}BurnOutStatR")
{
    field(DESC, "Burnout condition bit")
    field(INP,  "${CONTROLLER}${R}#Ch${CH}StatR.B6 CP MSS")

    field(ZNAM, "Nominal condition")
    field(ONAM, "Burnout condition")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(mbbi, "${P}${R}Boost-RB")
{
    field(DTYP, "stream")
    field(INP,  "@lm510.proto lhe_boost_get(${P}${R},ChanR,#esrBoostRB) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-RB")

    field(ZRVL, "0")
    field(ZRST, "OFF")

    field(ONVL, "1")
    field(ONST, "ON")

    field(TWVL, "2")
    field(TWST, "Smart")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrBoostRB, normalPV=Boost-RB"
include "lm510_check_esr.template"


record(mbbo, "${P}${R}BoostS")
{
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto lhe_boost_set(${P}${R},ChanR,#esrBoostS) ${ASYNPORT}")

    field(ZRVL, "0")
    field(ZRST, "OFF")

    field(ONVL, "1")
    field(ONST, "ON")

    field(TWVL, "2")
    field(TWST, "Smart")
    field(FLNK, "${P}${R}Boost-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrBoostS, normalPV=BoostS"
include "lm510_check_esr.template"


record(calcout, "${P}${R}#SamplIntvl-RB")
{
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto lhe_sample_interval_get(${P}${R},ChanR,#esrSamplIntvlRB) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-RB")

    field(CALC, "42")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrSamplIntvlRB, normalPV=#SamplIntvl-RB"
include "lm510_check_esr.template"


record(fanout, "${P}${R}#SamplIntvl-RB-FO")
{
    field(LNK1, "${P}${R}SamplIntvlHour-RB")
    field(LNK2, "${P}${R}SamplIntvlMin-RB")
    field(LNK3, "${P}${R}SamplIntvlSec-RB")

    field(DISV, "0")
    field(SDIS, "${P}${R}DisconnectTrigR CP")
}


record(ai, "${P}${R}SamplIntvlHour-RB")
{
    field(DESC, "Sampling interval hours")
    field(INP,  "${P}${R}#SamplIntvl-RB.A CP MSS")
    field(EGU,  "h")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}


record(ai, "${P}${R}SamplIntvlMin-RB")
{
    field(DESC, "Sampling interval minutes")
    field(INP,  "${P}${R}#SamplIntvl-RB.B CP MSS")
    field(EGU,  "min")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}


record(ai, "${P}${R}SamplIntvlSec-RB")
{
    field(DESC, "Sampling interval seconds")
    field(INP,  "${P}${R}#SamplIntvl-RB.C CP MSS")
    field(EGU,  "s")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}


record(bo, "${P}${R}SamplIntvlS")
{
    field(DESC, "Initiate sampling interval commit")
    field(FLNK, "${P}${R}#SamplIntvlS")
}


record(calcout, "${P}${R}#SamplIntvlS")
{
    field(DESC, "Commit sampling interval")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto lhe_sample_interval_set(${P}${R},ChanR,#esrSamplIntvlS) ${ASYNPORT}")
    field(INPA, "${P}${R}SamplIntvlHourS")
    field(INPB, "${P}${R}SamplIntvlMinS")
    field(INPC, "${P}${R}SamplIntvlSecS")

    field(FLNK, "${P}${R}#SamplIntvl-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrSamplIntvlS, normalPV=#SamplIntvlS"
include "lm510_check_esr.template"


record(ao, "${P}${R}SamplIntvlHourS")
{
    field(DESC, "Sampling interval hours")
    field(EGU,  "h")
    field(DRVL, "0")
    field(LOPR, "0")
    field(DRVH, "99")
    field(HOPR, "99")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}


record(ao, "${P}${R}SamplIntvlMinS")
{
    field(DESC, "Sampling interval minutes")
    field(EGU,  "min")
    field(DRVL, "0")
    field(LOPR, "0")
    field(DRVH, "59")
    field(HOPR, "59")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}


record(ao, "${P}${R}SamplIntvlSecS")
{
    field(DESC, "Sampling interval seconds")
    field(EGU,  "s")
    field(DRVL, "0")
    field(LOPR, "0")
    field(DRVH, "59")
    field(HOPR, "59")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}


record(bo, "${P}${R}SamplIntvlContS")
{
    field(DESC, "Set continuous sampling")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto lhe_sample_continuous_set(${P}${R},ChanR,#esrSamplIntvlContS) ${ASYNPORT}")
    field(FLNK, "${P}${R}#SamplIntvl-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrSamplIntvlContS, normalPV=SamplIntvlContS"
include "lm510_check_esr.template"


record(mbbi, "${P}${R}SamplMode-RB")
{
    field(DESC, "Get sampling mode")
    field(DTYP, "stream")
    field(INP,  "@lm510.proto lhe_sample_mode_get(${P}${R},ChanR,#esrSamplModeRB) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-RB")

    field(ZRVL, "0")
    field(ZRST, "OFF")

    field(ONVL, "1")
    field(ONST, "Sample/Hold")

    field(TWVL, "2")
    field(TWST, "Continuous")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrSamplModeRB, normalPV=SamplMode-RB"
include "lm510_check_esr.template"


record(mbbo, "${P}${R}SamplModeS")
{
    field(DESC, "Set sampling mode")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto lhe_sample_mode_get(${P}${R},ChanR,#esrSamplModeS) ${ASYNPORT}")

    field(ZRVL, "0")
    field(ZRST, "OFF")

    field(ONVL, "1")
    field(ONST, "Sample/Hold")

    field(TWVL, "2")
    field(TWST, "Continuous")

    field(FLNK, "${P}${R}SamplMode-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrSamplModeS, normalPV=SamplModeS"
include "lm510_check_esr.template"


record(stringin, "${P}${R}#CtrlRlyMode-RB")
{
    field(DESC, "Control relay mode helper")
    field(DTYP, "stream")
    field(INP,  "@lm510.proto control_relay_get(${P}${R},ChanR) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-RB")

    field(FLNK, "${P}${R}#CtrlRlyMode-RB-FO")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(fanout, "${P}${R}#CtrlRlyMode-RB-FO")
{
    field(LNK1, "${P}${R}#CtrlRlyActiveTimR")
    field(LNK2, "${P}${R}#ChkURng")
    field(LNK3, "${P}${R}#CtrlRlyActiveTim2R")
    field(LNK4, "${P}${R}CtrlRlyActiveTimR")
    field(LNK5, "${P}${R}#ChkCtrlRly")

# Do not disable this fanout
    field(DISV, "42")
    field(SDIS, "${P}${R}DisconnectTrigR CP")
}


# Convert time value string to double
record(ai, "${P}${R}#CtrlRlyActiveTimR")
{
    field(INP,  "${P}${R}#CtrlRlyMode-RB MSS")
    field(EGU,  "min")
}


# Convert relay mode string to double
# DBL(x) is used because:
#  Explicit conversion to a number (by DBL, INT, or NINT) is more aggressive than implicit conversion, and skips leading non-numeric characters.
# Make sure not to overwrite the initial conversion (ie, 'A') because iChkCtrlRly needs to check its value
record(scalcout, "${P}${R}#ChkURng")
{
    field(INAA, "${P}${R}#CtrlRlyMode-RB MSS")
    field(INPB, "-1")
    field(INPC, "${P}${R}#CtrlRlyActiveTimR.STAT")
    field(CALC, "A:=DBL(AA{'min',''}); B:=(C!=0 ? B : A); A")
}

# Read time value as converted by scalcout
record(ai, "${P}${R}#CtrlRlyActiveTim2R")
{
    field(INP,  "${P}${R}#ChkURng.B MSS")
}


record(ai, "${P}${R}CtrlRlyActiveTimR")
{
    field(DESC, "Time since CTRL started if relay active")
    field(INP,  "${P}${R}#CtrlRlyActiveTim2R")
    field(EGU,  "min")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


# 1: IF string is a number
# 2: otherwise
record(calcout, "${P}${R}#ChkCtrlRly")
{
    field(INPA, "${P}${R}#CtrlRlyActiveTimR.STAT")
    field(INPB, "${P}${R}#CtrlRlyActiveTimR.SEVR")
    field(CALC, "((A || B) == 0) ? 1 : 2")
    field(OUT,  "${P}${R}#CtrlRlyStatVR.SELN MSS PP")
}


# switch case
record(seq, "${P}${R}#CtrlRlyStatVR")
{
    field(SELM, "Specified")
    field(DISP, "1")

#   set status to Active
    field(DO1,  "0")
    field(LNK1, "${P}${R}CtrlRlyMode-RB MSS PP")

#   try to parse the mode string as ENUM
    field(DO2,  "1")
    field(LNK2, "${P}${R}#CtrlRlyParse.PROC")
}


# Initialize relay status to <<INVALID>>
# Forward process parsing mode string as ENUM
record(longout, "${P}${R}#CtrlRlyStatInit")
{
    field(DESC, "Set CtrlRlyMode-RB to INVALID")
    field(DISP, "1")
    field(VAL,  "3")
    field(OUT,  "${P}${R}CtrlRlyMode-RB PP")
}


# Write mode string to mode status ENUM
record(stringout, "${P}${R}#CtrlRlyParse")
{
    field(DESC, "Parse mode string")
    field(DOL,  "${P}${R}#CtrlRlyMode-RB MSS")
    field(OMSL, "closed_loop")
    field(OUT,  "${P}${R}CtrlRlyMode-RB MSS PP")

    # Check if string could be written
    field(FLNK, "${P}${R}#ChkRlyParse")
}


record(calcout, "${P}${R}#ChkRlyParse")
{
    field(DESC, "Check if #CtrlRlyParse was successful")
    field(INPA, "${P}${R}#CtrlRlyParse.SEVR")
    field(INPB, "${P}${R}#CtrlRlyParse.STAT")
# If #CtrlRlyParse is INVALID/LINK that means write to CtrlRlyMode-RB was not successful i.e. #CtrlRlyMode-RB is not something we recognize
    field(CALC, "A == 3 && B == 14")
    field(OOPT, "When Non-zero")
# Set CtrlRlyMode-RB to INVALID
    field(OUT,  "${P}${R}#CtrlRlyStatInit.PROC PP")
}


record(mbbi, "${P}${R}CtrlRlyMode-RB")
{
    field(DESC, "Status of control relay mode")

    field(ZRVL, "0")
    field(ZRST, "Active")

    field(ONVL, "1")
    field(ONST, "Off")

    field(TWVL, "2")
    field(TWST, "Timeout")

    field(THVL, "3")
    field(THST, "<<INVALID>>")

    field(UNSV, "MAJOR")

    field(VAL,  "3")
    field(PINI, "YES")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(mbbo, "${P}${R}CtrlRlyModeS")
{
    field(DESC, "Set control relay mode")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto control_relay_set(${P}${R},ChanR,#esrCtrlRlyModeS) ${ASYNPORT}")

    field(ZRVL, "0")
    field(ZRST, "OFF")

    field(ONVL, "1")
    field(ONST, "Manual")

    field(TWVL, "2")
    field(TWST, "Auto")

    field(FLNK, "${P}${R}#CtrlRlyMode-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrCtrlRlyModeS, normalPV=CtrlRlyModeS"
include "lm510_check_esr.template"


record(ai, "${P}${R}HighAlrmThresh-RB")
{
    field(DESC, "High alarm threshold")
    field(DTYP, "stream")
    field(INP,  "@lm510.proto high_alarm_threshold_get(${P}${R},ChanR,#esrHighAlrmThreshRB) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-RB")

# EGU is set by the protocol
    field(PREC, "1")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrHighAlrmThreshRB, normalPV=HighAlrmThresh-RB"
include "lm510_check_esr.template"


record(ao, "${P}${R}HighAlrmThreshS")
{
    field(DESC, "High alarm threshold")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto high_alarm_threshold_set(${P}${R},ChanR,#esrHighAlrmThreshS) ${ASYNPORT}")

# EGU is set by #EGUChRec
    field(PREC, "1")
    field(FLNK, "${P}${R}HighAlrmThresh-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrHighAlrmThreshS, normalPV=HighAlrmThreshS"
include "lm510_check_esr.template"


record(bo, "${P}${R}HighAlrmThreshDisS")
{
    field(DESC, "Disable high alarm")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto high_alarm_threshold_disable(${P}${R},ChanR,#esrHighAlrmThreshDisS) ${ASYNPORT}")

    field(FLNK, "${P}${R}HighAlrmThresh-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrHighAlrmThreshDisS, normalPV=HighAlrmThreshDisS"
include "lm510_check_esr.template"


record(ai, "${P}${R}HighCtrlThresh-RB")
{
    field(DESC, "Control function high threshold")
    field(DTYP, "stream")
    field(INP,  "@lm510.proto high_control_threshold_get(${P}${R},ChanR,#esrHighCtrlThreshRB) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-RB")

# EGU is set by the protocol
    field(PREC, "1")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrHighCtrlThreshRB, normalPV=HighCtrlThresh-RB"
include "lm510_check_esr.template"


record(ao, "${P}${R}HighCtrlThreshS")
{
    field(DESC, "Control function high threshold")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto high_control_threshold_set(${P}${R},ChanR,#esrHighCtrlThreshS) ${ASYNPORT}")

# EGU is set by #EGUChRec
    field(PREC, "1")
    field(FLNK, "${P}${R}HighCtrlThresh-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrHighCtrlThreshS, normalPV=HighCtrlThreshS"
include "lm510_check_esr.template"


record(ai, "${P}${R}LowAlrmThresh-RB")
{
    field(DESC, "Low alarm threshold")
    field(DTYP, "stream")
    field(INP,  "@lm510.proto low_alarm_threshold_get(${P}${R},ChanR,#esrLowAlrmThreshRB) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-RB")

# EGU is set by the protocol
    field(PREC, "1")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrLowAlrmThreshRB, normalPV=LowAlrmThresh-RB"
include "lm510_check_esr.template"


record(ao, "${P}${R}LowAlrmThreshS")
{
    field(DESC, "Low alarm threshold")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto low_alarm_threshold_set(${P}${R},ChanR,#esrLowAlrmThreshS) ${ASYNPORT}")

# EGU is set by #EGUChRec
    field(PREC, "1")
    field(FLNK, "${P}${R}LowAlrmThresh-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrLowAlrmThreshS, normalPV=LowAlrmThreshS"
include "lm510_check_esr.template"


record(bo, "${P}${R}LowAlrmThreshDisS")
{
    field(DESC, "Disable low alarm")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto low_alarm_threshold_disable(${P}${R},ChanR,#esrLowAlrmThreshDisS) ${ASYNPORT}")

    field(FLNK, "${P}${R}LowAlrmThresh-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrLowAlrmThreshDisS, normalPV=LowAlrmThreshDisS"
include "lm510_check_esr.template"


record(ai, "${P}${R}LowCtrlThresh-RB")
{
    field(DESC, "Control function low threshold")
    field(DTYP, "stream")
    field(INP,  "@lm510.proto low_control_threshold_get(${P}${R},ChanR,#esrLowCtrlThreshRB) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-RB")

# EGU is set by the protocol
    field(PREC, "1")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrLowCtrlThreshRB, normalPV=LowCtrlThresh-RB"
include "lm510_check_esr.template"


record(ao, "${P}${R}LowCtrlThreshS")
{
    field(DESC, "Control function low threshold")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto low_control_threshold_set(${P}${R},ChanR,#esrLowCtrlThreshS) ${ASYNPORT}")

# EGU is set by #EGUChRec
    field(PREC, "1")
    field(FLNK, "${P}${R}LowCtrlThresh-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrLowCtrlThreshS, normalPV=LowCtrlThreshS"
include "lm510_check_esr.template"


record(ai, "${P}${R}SensorLen-RB")
{
# Keep it for backwards compatibility until all the OPIs
# in use are updated
alias("${P}${R}SensorLenR")
    field(DESC, "Query sensor length")
    field(DTYP, "stream")
    field(INP,  "@lm510.proto sensor_length_get(${P}${R},ChanR,#esrSensorLenRB) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-RB")

# EGU is set by the protocol
    field(PREC, "1")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrSensorLenRB, normalPV=SensorLen-RB"
include "lm510_check_esr.template"


record(ao, "${P}${R}SensorLenS")
{
    field(DESC, "Set sensor length")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto sensor_length_set(${P}${R},ChanR,#esrSensorLenS) ${ASYNPORT}")
    field(EGU,  "cm")
    field(PREC, "1")
    field(FLNK, "${P}${R}SensorLen-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrSensorLenS, normalPV=SensorLenS"
include "lm510_check_esr.template"


record(ai, "${P}${R}LvlR")
{
    field(DESC, "Level reading")
    field(DTYP, "stream")
    field(INP,  "@lm510.proto meas_get(${P}${R},ChanR) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${CONTROLLER}-1")

# EGU is set by the protocol
    field(PREC, "1")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(bo, "${P}${R}MeasS")
{
    field(DESC, "Initiate level reading")
    field(DTYP, "stream")
    field(OUT,  "@lm510.proto meas_start(${P}${R},ChanR,#esrMeasS) ${ASYNPORT}")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsCtrlMode")
}
substitute "esrPV=#esrMeasS, normalPV=MeasS"
include "lm510_check_esr.template"
