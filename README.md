lm510
======

European Spallation Source ERIC Site-specific EPICS module : lm510

This module provides device support for the LM510 liquid cryogen level monitor from Cryomagnetics Inc

- Manufacturer page: [link](https://www.cryomagnetics.com/products/model-lm-510-liquid-cryogen-monitor/)
- Manual: [LM-510 Rev. 1.2.pdf](docs/LM-510 Rev. 1.2.pdf)

Dependencies
============

*  `stream`


Using the module
================
In order to use the device support module declare a require statement for streamdevice and lm510:

```
    require stream
    require lm510
```
    
Then load the database for the controller device (`TS2-010CRM:Cryo-LC-001`):

```
    iocshLoad("$(lm510_DIR)/lm510.iocsh", "DEVICENAME=TS2-010CRM:Cryo-LC-001, IPADDR=ts2-level-controller")
```

Parameters:
- DEVICENAME: The name of the device as registered in the Naming service
- IPADDR: The IP address of your controller
- PORT (optional): The Ethernet port. Default: 4266

Then load the database for the sensor (`TS2-010CRM:Cryo-LT-001`):

```
    iocshLoad("$(lm510_DIR)/lm510_lt.iocsh", "DEVICENAME=TS2-010CRM:Cryo-LT-001, CONTROLLER=TS2-010CRM:Cryo-LC-001, CHANNEL=1")
```

Parameters:
- DEVICENAME: The name of the device as registered in the Naming service
- CONTROLLER: The devicename of the controller
- CHANNEL: Channel on the head unit where the level transmitter is connected to. Possible values: 1, 2


Building the module
===================
This module should compile as a regular EPICS module.

* Run `make` from the command line.
